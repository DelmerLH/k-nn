class Knn
    attr_accessor :data, :fit_list, :test_list, :stats_k1, :stats_k3, :stats_k5, :stats_k10

    def initialize(data)
        @data = data
        @fit_list = []
        @test_list = []
        @stats_k1 = [0,0,0.0]
        @stats_k3 = [0,0,0.0]
        @stats_k5 = [0,0,0.0]
        @stats_k10 = [0,0,0.0]
        select_test_data
        select_fit_data
    end

    def select_test_data
        while @test_list.length < 100
            index = rand(data.length-1)
            @test_list << data[index]
            @data.delete_at(index)
        end
    end
    
    def select_fit_data
        @fit_list = @data
    end

    def run_knn
        @fit_list.each do |fit_row|
            nearest_neighbor = []
            @test_list.each do |test_row|
                calculate_distance(fit_row, test_row)
            end

            sort_list(@test_list)

            for i in 0..9
                nearest_neighbor << @test_list[i]
            end

            check_success(nearest_neighbor, fit_row, 1)
            check_success(nearest_neighbor, fit_row, 3)
            check_success(nearest_neighbor, fit_row, 5)
            check_success(nearest_neighbor, fit_row, 10)
        end
        calculate_percentage_success
        print_stats
    end

    def check_success(nearest_neighbor, fit_row, k)
        times_succeed = 0
        times_failed = 0
        for i in 0..k-1
            if nearest_neighbor[i].iris_class == fit_row.iris_class
                times_succeed += 1
            else
                times_failed += 1
            end
        end
        if k == 1
            @stats_k1[0] += times_succeed
            @stats_k1[1] += times_failed
        elsif k == 3
            @stats_k3[0] += times_succeed
            @stats_k3[1] += times_failed
        elsif k == 5
            @stats_k5[0] += times_succeed
            @stats_k5[1] += times_failed
        else
            @stats_k10[0] += times_succeed
            @stats_k10[1] += times_failed
        end
    end
                

    def sort_list(list)
        for i in 1..list.length-1
            j = i
            while j > 0 && list[j-1].distance > list[j].distance
                aux = list[j]
                list[j] = list[j-1]
                list[j-1] = aux
                j -= 1
            end
        end
    end
                

    def calculate_distance(fit_row, test_row)
        summation = 0.0
        for i in 0..fit_row.atributes_values.length-1
            summation += (fit_row.atributes_values[i]-test_row.atributes_values[i])**2
        end
        distance = Math.sqrt(summation)
        test_row.distance = distance
    end 

    def calculate_percentage_success
        @stats_k1[2] = (@stats_k1[0].to_f / (@stats_k1[0].to_f + @stats_k1[1].to_f))*100
        @stats_k3[2] = (@stats_k3[0].to_f / (@stats_k3[0].to_f + @stats_k3[1].to_f))*100
        @stats_k5[2] = (@stats_k5[0].to_f / (@stats_k5[0].to_f + @stats_k5[1].to_f))*100
        @stats_k10[2] = (@stats_k10[0].to_f / (@stats_k10[0].to_f + @stats_k10[1]).to_f)*100
    end

    def print_stats
        puts "K\tSuccess\tFails\tPercentage of success"
        puts "1\t#{@stats_k1[0]}\t#{@stats_k1[1]}\t#{@stats_k1[2]}%"
        puts "3\t#{@stats_k3[0]}\t#{@stats_k3[1]}\t#{@stats_k3[2]}%"
        puts "5\t#{@stats_k5[0]}\t#{@stats_k5[1]}\t#{@stats_k5[2]}%"
        puts "10\t#{@stats_k10[0]}\t#{@stats_k10[1]}\t#{@stats_k10[2]}%"
    end
end