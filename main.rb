require_relative 'row'
require_relative 'knn.rb'

data = File.read("iris.data")
data_splitted = data.split

data_set = []

data_splitted.each do |row|
    row_s = row.split(',')
    data_set << row_s
end

database = []

data_set.each do |row|
    atributes = []
    atributes << row[0].to_f << row[1].to_f << row[2].to_f << row[3].to_f
    iris_class = row[4]
    row_data = Row.new(atributes, iris_class)
    database << row_data
end

knn = Knn.new(database)
knn.run_knn
